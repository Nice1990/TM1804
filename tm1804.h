/**
 *      STM32F103xx系列单总线灯带驱动
 * 
 * 适用：TM1804、ws2812、ws2813
 * 注意：
 *      1. 采用nop软件延时，主频72MHz
 *      2. 使用原子sys.h库文件
 *      3. 使用MDK，优化等级0
 * 特别说明：
 *      1. spi单字节发送无意义，需要使用dma连续发送，耗费大量内存，待完善
 *      2. nop延时过程中会短暂关闭中断，时间极短
 * 
 * 开源协议： Apache v2.0
 * By： Lee，2021年3月18日
*/

#ifndef _TM1804_H
#define _TM1804_H

#include "main.h"
#include "sys.h"

#define USE_WS2812 1

#if USE_WS2812
//WS2812 驱动RGB 颜色定义  GRB
#define COLOR_BLACK 0x000000                      //黑色
#define COLOR_R 0x00ff00                          //红色
#define COLOR_G 0xff0000                          //绿色
#define COLOR_B 0x0000ff                          //蓝色
#define COLOR_YELLOW (COLOR_R | COLOR_G)          //黄色
#define COLOR_MAGENTA (COLOR_R | COLOR_B)         //洋红色
#define COLOR_CYAN (COLOR_G | COLOR_B)            //青色
#define COLOR_WHITE (COLOR_R | COLOR_G | COLOR_B) //白色

#else
//TM1804 驱动RGB 颜色定义  GRB
#define COLOR_BLACK 0x000000                      //黑色
#define COLOR_R 0xff0000                          //红色
#define COLOR_G 0x00ff00                          //绿色
#define COLOR_B 0x0000ff                          //蓝色
#define COLOR_YELLOW (COLOR_R | COLOR_G)          //黄色
#define COLOR_MAGENTA (COLOR_R | COLOR_B)         //洋红色
#define COLOR_CYAN (COLOR_G | COLOR_B)            //青色
#define COLOR_WHITE (COLOR_R | COLOR_G | COLOR_B) //白色
#endif

//类型定义
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

//接口定义 修改调用方式会改变延时
#define TM1804_DIN(_out) \
    {                    \
        PAout(2) = _out; \
    }
#define TM1804_GPIO_PIN GPIO_PIN_2 // 对应修改 TM1804_DIN
#define TM1804_GPIO GPIOA
#define TM1804_CLOCK_ENABLE __HAL_RCC_GPIOA_CLK_ENABLE
#define TM1804_DISABLE_IRQ() __disable_irq()
#define TM1804_ENABLE_IRQ() __enable_irq()

//功能定义
#define SOFT_WAVEFORM 1 //使用软件模拟波形，72MHz主频，MDK优化等级0

#if !SOFT_WAVEFORM
#define SPI_WAVEFORM 1 //使用SPI模拟波形，9M spi时钟频率，72MHz主频
#endif

//操作API
void TM1804_SendReset(void);
void TM1804_SendOneColor(u32 color_t);
void TM1804_SendOneColor_Not(u32 color_t);

//使用API
void TM1804_Init(void);
void TM1804_ArrayOut(u32 *tab_p, u8 len);
#endif
