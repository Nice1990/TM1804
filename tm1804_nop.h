#ifndef _TM1804_NOP_H
#define _TM1804_NOP_H

//0信号高电平时间可很短，不可太长，容易识别为1信号
#define TM1804_WRITE_0H_NOP() \
    {                         \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
    }
//0信号对应的低电平时间   （72MHz,mdk优化等级0，+TM1804_SendOneColor函数内部耗时）
#define TM1804_WRITE_0L_NOP() \
    {                         \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
    }

//1高电平时间850ns（=0低电平时间） （72MHz,mdk优化等级0，+TM1804_SendOneColor函数内部耗时）
#define TM1804_WRITE_1H_NOP() \
    {                         \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
        __nop();              \
    }
//1信号对应的低电平时间   （72MHz,mdk优化等级0，+TM1804_SendOneColor函数内部耗时）
#define TM1804_WRITE_1L_NOP() \
    {                         \
        __nop();              \
        __nop();              \
    }

//SPI高电平可以确定，但低电平时间不够，需要NOP延时：SPI的1字节时间=0.889us（9MHz），nop延时=1.25-spi时间(72MHz,26个nop测试1.25刚好)
#define TM1804_SPI_NOP() \
    {                    \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
        __nop();         \
    }

#endif
