/*
单线通讯方式，采用归零码 24bit RGB，高位先发，RESET信号前，OUTR、OUTG、OUTB管脚原输出保持不变，
    24bit接收后级联输出后续收到的数据，结束时发送RESET信号
    高速模式时: 数据 Bit 位周期为 1.25μs,800K
    低速模式时: 数据 Bit 位周期为 2.5μs,400K
  
    适用：TM1804、ws2812、ws2813

    注意：只需要满足高电平时间+频率低于800K即可，RESET信号=低电平时间>10us(大约)
*/

#include "tm1804.h"
#include "tm1804_nop.h"

// -----------------------------------Soft----------------------------------------------------------
#if SOFT_WAVEFORM // 软件模拟波形

/**
 * 数据0必须严格保证时序，否则会导致闪烁
 * 采用发送0高电平时禁用所用中断方式
 * 禁用时间：< 20 指令周期
*/
#if 1
#define TM1804_WRITE_0()       \
    {                          \
        TM1804_DISABLE_IRQ();  \
                               \
        TM1804_DIN(1);         \
        TM1804_WRITE_0H_NOP(); \
        TM1804_DIN(0);         \
                               \
        TM1804_ENABLE_IRQ();   \
                               \
        TM1804_WRITE_0L_NOP(); \
    }
#else
#define TM1804_WRITE_0()       \
    {                          \
        TM1804_DIN(1);         \
        TM1804_WRITE_0H_NOP(); \
        TM1804_DIN(0);         \
        TM1804_WRITE_0L_NOP(); \
    }
#endif

#define TM1804_WRITE_1()       \
    {                          \
        TM1804_DIN(1);         \
        TM1804_WRITE_1H_NOP(); \
        TM1804_DIN(0);         \
        TM1804_WRITE_1L_NOP(); \
    }

/**
 * 初始化，配置IO口为推挽
 */
void TM1804_Init(void)
{
    GPIO_InitTypeDef GPIO_Initure;

    // 配置单线IO口
    TM1804_CLOCK_ENABLE();                                           // 开启GPIO时钟
    HAL_GPIO_WritePin(TM1804_GPIO, TM1804_GPIO_PIN, GPIO_PIN_RESET); // 默认电平
    GPIO_Initure.Pin = TM1804_GPIO_PIN;
    GPIO_Initure.Mode = GPIO_MODE_OUTPUT_PP;   // 推挽输出
    GPIO_Initure.Pull = GPIO_NOPULL;           // 上拉
    GPIO_Initure.Speed = GPIO_SPEED_FREQ_HIGH; // 速
    HAL_GPIO_Init(TM1804_GPIO, &GPIO_Initure); // PB初始化

    TM1804_SendReset(); // 发送一个复位信号，首次复位
}

/**
 * 发送复位信号，更新数据
 * 说明：低电平持续最小时间即可
 */
void TM1804_SendReset(void)
{
    u32 t = 350; // 测试29us   >10us即可
    TM1804_DIN(0);
    while (t--)
        ; // 汇编-5周期
}

/**
 * 发送一个24bitRGB颜色数据
 */
void TM1804_SendOneColor(u32 color_t)
{
    u8 i;

    for (i = 0; i < 24; i++)
    {
        if ((color_t & 0x800000u)) // 高位先发
        {
            TM1804_WRITE_1(); // 反相,发送的位置会影响延时(for循环语句耗时)
        }
        else
        {
            TM1804_WRITE_0();
        }
        color_t <<= 1;
    }
}

/**
 * 反相发送一个24bitRGB颜色数据
 */
void TM1804_SendOneColor_Not(u32 color_t)
{
    u8 i;

    for (i = 0; i < 24; i++)
    {
        if ((color_t & 0x800000u) == 0) // 高位先发
        {
            TM1804_WRITE_1(); // 反相,发送的位置会影响延时(for循环语句耗时)
        }
        else
        {
            TM1804_WRITE_0();
        }
        color_t <<= 1;
    }
}

// ----------------------------End Soft----------------------------------------------------

// ----------------------------Spi----------------------------------------------------
#elif SPI_WAVEFORM

/*
SPI模拟方式：
  9M时钟频率，72M主频
  一字节数据控制0.889us，加上nop延时构成1.25us
  SPI发送数据最高位必须=0
*/

#include "spi.h"
#define SPI_HANDLE hspi3 // 使用的SPI句柄

/**
 * 初始化
 * 配置IO口为推挽,启用外设
 */
void TM1804_Init(void)
{
    GPIO_InitTypeDef GPIO_Initure;

    // 配置单线IO口
    __HAL_RCC_GPIOB_CLK_ENABLE();                         // 开启GPIOE时钟
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET); // 默认电平
    GPIO_Initure.Pin = GPIO_PIN_9;
    GPIO_Initure.Mode = GPIO_MODE_OUTPUT_PP;   // 推挽输出
    GPIO_Initure.Pull = GPIO_PULLUP;           // 上拉
    GPIO_Initure.Speed = GPIO_SPEED_FREQ_HIGH; // 速
    HAL_GPIO_Init(GPIOB, &GPIO_Initure);       // PB初始化

    __HAL_SPI_ENABLE(&SPI_HANDLE);
    TM1804_SendReset(); // 发送一个复位信号，首次复位
}

/**
 * 发送复位信号，更新数据
 * 说明：低电平持续最小时间即可
 */
void TM1804_SendReset(void)
{
    u32 t = 350; // 测试29us   >10us即可
    u8 tab = 0;

    HAL_SPI_Transmit(&SPI_HANDLE, &tab, 1, 100);
    while (t--)
        ; // 汇编-5周期
}

void TM1804_SendOneColor(u32 color_t)
{
    u8 i;
    u8 dat_t[] = {0x78, 0x7f};

    for (i = 0; i < 24; i++)
    {
        while (__HAL_SPI_GET_FLAG(&SPI_HANDLE, SPI_FLAG_TXE) == 0)
            ;
        TM1804_SPI_NOP(); // 特别注意，驱动频率不能高于800K，所以增加延时
        SPI_HANDLE.Instance->DR = dat_t[!!(color_t & 0x800000)];
        color_t <<= 1;
    }
}

void TM1804_SendOneColor_Not(u32 color_t)
{
    u8 i;
    u8 dat_t[] = {0x78, 0x7f};

    for (i = 0; i < 24; i++)
    {
        {
            while (__HAL_SPI_GET_FLAG(&SPI_HANDLE, SPI_FLAG_TXE) == 0)
                ;
            TM1804_SPI_NOP(); // 特别注意，驱动频率不能高于800K，所以增加延时
            SPI_HANDLE.Instance->DR = dat_t[!(color_t & 0x800000)];
            //  HAL_SPI_Transmit(&SPI_HANDLE,&dat_t[!(color_t&0x800000)],1,100);    // 耗时严重，每次调用5us
        }
        color_t <<= 1;
    }
}

// ----------------------------End Spi----------------------------------------------------
#endif

/**
 * 发送并输出RGB数组数据
 * @1：32Bit数组指针
 * @2：数组长度，单位u32
 * 注意：此函数执行过程中会关闭中断，按需修改
 */
void TM1804_ArrayOut(u32 *tab_p, u8 len)
{
    u8 i;

    for (i = 0; i < len; i++)
    {
        TM1804_SendOneColor(tab_p[i]);
    }

    TM1804_SendReset(); // 复位信号，固定时间调用时，可不加
}
